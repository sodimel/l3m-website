# l3m-website

Mon site personnel, écrit avec du php moche et des classes moches. C'est la 5ème refonte.

<div align="center"><a href="https://git.bitmycode.com/sodimel/l3m-website/uploads/5bdbbefd55e7d2002e95c55c6ee9960a/image.png"><img src="https://git.bitmycode.com/sodimel/l3m-website/uploads/714ccafa5ab19919e3aff568dc69f012/l3m.png" alt="Screenshot (cliquer pour agrandir)" title="Screenshot (cliquer pour agrandir" /></a></div>

----

# Installer

Voilà comment on peut setup et lancer le site pour un développement local. Les instructions peuvent être faussées ou incomplètes, j'ai pas vraiment le temps de tout bien tester et j'ai fait la manip ya déjà quelques temps.

## Setup l'environnement

1. Installer tous les paquets bdd & apache :
    ```bash
    sudo apt install mariadb-server apache2
    ```
2. Installer tous les paquets php :
    ```bash
    sudo apt install php8.1-mysql php8.1-intl php8.1-fpm php8.1 php8.1-xml php-mbstring
    ```
3. Activer les extensions apache :
    ```bash
    sudo a2enmod rewrite proxy_fcgi expires
    ```
4. Activer mariadb, lancer mariadb, activer apache, lancer apache :
    ```bash
    sudo systemctl enable apache2
    sudo systemctl start apache2 # ou alors sudo systemctl reload apache2

    sudo systemctl enable mariadb
    sudo systemctl start mariadb # ou alors sudo systemctl reload mariadb
    ```

## Lancer le site

1. Cloner le repo
2. Créer une bdd (`create database machin; exit;`)
3. Copier le fichier `site/models/config-dist.php` vers `site/models/config.php`.
4. Remplir le fichier.
    * *Exemple de fichier pour utilisation locale :*
        ```php
            <?php
            // config file
            // database config
            define('dbAddr', 'localhost');
            define('dbName', 'machin');
            define('dbUser', 'user');
            define('dbPass', 'password');


            // login config

            define('user', 'user');
            define('pass', 'pass');

            // rss config
            define('RSSBLOG', __DIR__ .'/../../rss/blog.xml');
            define('RSSPROJECTS', __DIR__ .'/../../rss/projects.xml');

            // sitemap
            define('SITEMAP', __DIR__ .'/../../sitemap/sitemap.xml');
        ```
        (bien s'assurer que les fichiers existent et qu'ils peuvent être édités par php)

5. Initialiser la base à partir des schémas :
    ```bash
    sudo mariadb -p machin < l3m.sql
    ```
6. Créer un fichier `/etc/apache2/sites-enabled/l3m-dev.conf` :
    ```conf
    <VirtualHost *:80>
        ServerName l3m.local
        ServerAdmin corentin@244466666.xyz
        DocumentRoot /var/www/html/l3m-website/

        Options FollowSymLinks MultiViews

        # redirect old uploaded files to new upload page
        RedirectMatch /p/up/files/(.+)$ https://up.l3m.in/file/$1

        ErrorDocument 404 /index.php?action=404
        RedirectMatch 404 /\.git

        # Add correct content-type for fonts
        AddType application/x-font-ttf .ttf
        # Compress compressible fonts
        AddOutputFilterByType DEFLATE application/x-font-ttf

        ExpiresActive On
        ExpiresByType image/png "access plus 1 year"
        ExpiresByType text/css "access plus 1 year"
        ExpiresByType text/javascript "access plus 1 year"
        ExpiresByType application/x-font-ttf "access plus 1 year"

        RewriteEngine On
        RewriteRule ^/(about|changelog|rss|projects|contact|patate|eplucher)$ /index.php?action=$1
        RewriteRule ^/rss/(blog|projets)$ /rss/$1.xml
        RewriteRule ^/tag/([-a-z0-9]+)$ /index.php?action=tags&title=$1
        RewriteRule ^/page/([0-9]+)$ /index.php?action=blog&page=$1
        RewriteRule ^/article/([-a-z0-9]+)$ /index.php?action=blog&title=$1
        RewriteRule ^/project/([-a-z0-9]+)$ /index.php?action=project&title=$1
        RewriteRule ^/patate/(blog|project)/submit$ /index.php?action=patate&type=$1submit
        RewriteRule ^/patate/(blog|project)/([-a-z0-9]+)/(edit|delete)$ /index.php?action=patate&type=$1$3&title=$2
    </VirtualHost>
    ```
7. *(optionnel)* Rajouter la ligne suivante dans `/etc/hosts` pour accéder au site de dev via sa propre url trop choupi :
    ```
    127.0.0.1       l3m.local
    ```
8. Reload apache pour qu'il lance le site à partir de sa config et qu'on puisse y accéder :
    ```bash
    sudo systemctl reload apache2
    ```
9. *(optionnel)* Trouver les erreurs et les débugger :D