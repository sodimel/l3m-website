<?php

// session pour quand on est admin
session_start();

// on active le output buffering pour réduire le ttfb
ob_start();

// error_reporting(E_ALL | E_STRICT);
// ini_set('display_startup_errors',1);
// ini_set('display_errors',1);


// le gros gros controlleur (peut-être le casser en plusieurs morceaux plus tard)
require_once('site/controller/mainController.php');

// le moins gros controlleur (oui c'est un morceau)
require_once('site/controller/adminController.php');

// on récupère le nom de la page ici (avant d'inclure le layout)
$pageName = getPageName();

// meta title
$pageTitle = getPageTitle();

// meta description
$description = getDescription();

// afficher le contenu (qui va switcher sur le contenu dynamique à afficher)
require_once('site/layout/layout.php');
?>
