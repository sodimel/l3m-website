<?php
class AdminManager extends Base
{
	public function getBlogPosts()
	{
		$blogpost = new BlogManager(null, null, null, null, null, null, null, null, null, null);
		return $blogpost->getAll();
	}

	public function getProjects()
	{
		$project = new ProjectManager(null, null, null, null, null, null, null, null);
        return $project->getProjects();
	}

	public function getBlogPost($link)
	{
		$blogpost = new BlogManager(null, null, null, null, null, null, null, null, null);
		return $blogpost->get($link);
	}

	public function getProject($link)
	{
        $project = new ProjectManager(null, null, null, null, null, null, null, null);
        return $project->get($link);
	}


	public function verifyAdminConnect()
	{
		return isset($_SESSION['user']) && $_SESSION['user'] == user;
	}

	public function updateBlogPost()
	{
		$blog = new BlogManager($_POST['time'], $_POST['title'], $_POST['cover'], $_POST['alt'], $_POST['content'], $_POST['shortContent'], $_POST['id'], $_POST['tags']);
		$blog->update();

		$rss = new RssManager();
		$rss->updateRssBlog();
		$sitemap = new SitemapManager();
		$sitemap->updateSitemap();
	}

	public function updateProject()
	{
		$project = new ProjectManager($_POST['name'], $_POST['link'], $_POST['cover'], $_POST['src'], $_POST['content'], $_POST['shortContent'], $_POST['id']);
		$project->update();

		$rss = new RssManager();
		$rss->updateRssProjects();
		$sitemap = new SitemapManager();
		$sitemap->updateSitemap();
	}

	public function sendBlogPost()
	{
		$blog = new BlogManager($_POST['time'], $_POST['title'], $_POST['cover'], $_POST['src'], $_POST['content'], $_POST['shortContent'], null, $_POST['tags']);
		$blog->send();

		$rss = new RssManager();
		$rss->updateRssBlog();
		$sitemap = new SitemapManager();
		$sitemap->updateSitemap();
	}


	public function sendProject()
	{
		$project = new ProjectManager($_POST['name'], $_POST['link'], $_POST['cover'], $_POST['src'], $_POST['content'], $_POST['shortContent'], null);
		$project->send();

		$rss = new RssManager();
		$rss->updateRssProjects();
		$sitemap = new SitemapManager();
		$sitemap->updateSitemap();
	}

	public function deleteBlogPost($link)
	{
		$blog = new BlogManager(null, null, null, null, null, null, null, null, null);
		$blog->delete($link);

		$rss = new RssManager();
		$rss->updateRssBlog();
		$sitemap = new SitemapManager();
		$sitemap->updateSitemap();
	}

	public function deleteProject($link)
	{
		$project = new ProjectManager(null, null, null, null, null, null, null, null);
		$project->delete($link);

		$rss = new RssManager();
		$rss->updateRssProjects();
		$sitemap = new SitemapManager();
		$sitemap->updateSitemap();
	}

}