<?php
class TagManager extends Base
{
	// envoi des tags d'un post de blog
	public function addTag($tags, $post)
	{

		// remplace "tag1, tag2 , tag3,tag4, tag5" en "tag1,tag2,tag3,tag4,tag5"
		$tags = preg_replace('( ,|, )', ',', $tags);
		// crée le tableau tags = ["tag1", "tag2", ...]
		$tags = explode(',', $tags);

		if($tags[0] != ''){
			// on ajoute chaque tag dans les tables correspondantes
			foreach ($tags as $tag)
			{
				$slug  = $this->slugify($tag, "tag");
				$idTag = $this->exist($tag);
				// si le tag n'existe pas, on l'ajoute dans la liste des tags
				if(!$idTag)
				{
					$req = self::$db->prepare('INSERT INTO l3m_tag(tag, slug) VALUES(:tag, :slug)');
					$req->execute([
						'tag' => $tag,
						'slug' => $slug
					]);

					// on sait que c'est ce tag qui vient d'être ajouté
					$idTag = self::$db->lastInsertId();
					
				}
				// on crée une relation entre le post et le tag
				$req = self::$db->prepare('INSERT INTO l3m_tag_post(tag, post) VALUES(:tag, :post)');
				$req->execute([
					'tag' => $idTag,
					'post' => $post
				]);
			}
		}
	}

	// supprime toutes les relations entre les tags et un post de blog ($post)
	function deleteTag($post)
	{
		$req = self::$db->prepare('DELETE FROM l3m_tag_post WHERE l3m_tag_post.post = ?');
		$req->execute([$post]);
	}

	// retourne l'id du tag s'il existe, false sinon
	function exist($tag)
	{
		$req = self::$db->prepare('SELECT id FROM l3m_tag WHERE tag = ?');
		$req->execute([$tag]);
		$new = $req->fetch();

		if(!$new)
			return false;
		return $new['id'];
	}

	// permet de récupérer les tags pour les afficher
	function getTags($id)
	{
		$req = self::$db->prepare('SELECT l3m_tag.tag, l3m_tag.slug FROM l3m_tag, l3m_tag_post WHERE l3m_tag.id = l3m_tag_post.tag AND l3m_tag_post.post = ?');
		$req->execute([$id]);
		$tags = $req->fetchAll();

		if(!$tags)
			return false;
		return $tags;
	}

	public function getTagsSitemap()
	{
		$req = self::$db->query('SELECT slug FROM l3m_tag');
		return $req;
	}
}