<?php
class RssManager extends Base
{
	function updateRssBlog()
	{
		$contentBlog =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
				<rss version=\"2.0\">
					<channel>
						<title>l3m website - posts de blog</title>
						<description>Site perso de Corentin Bettiol.</description>
						<lastBuildDate>". date('d/m/Y à H:i') ."</lastBuildDate>
						<link>https://l3m.in</link>";


		$blog = new BlogManager(null, null, null, null, null, null, null, null, null);
		$posts = $blog->getBlogRss();

		foreach ($posts as $post) {
			$post['datetime'] = new DateTime($post['time']);
			$date = date_format($post['datetime'], 'd/m/Y à H:i');
			$post_short_content = str_replace(['<', '>', '&',], ['&lt;', '&gt;', '&amp;',], $post['shortContent']);
			$contentBlog .= "<item>
							<title>". $post['title'] ."</title>
							<description>". $post_short_content ."</description>
							<pubDate>". $date ."</pubDate>
							<link>https://l3m.in/article/". $post['slug'] ."</link>
						</item>";
		}

		$contentBlog .= "</channel></rss>";

		file_put_contents(RSSBLOG, $contentBlog);
	}

	function updateRssProjects()
	{
		$contentProject =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
				<rss version=\"2.0\">
					<channel>
						<title>l3m website - projets</title>
						<description>Site perso de Corentin Bettiol.</description>
						<lastBuildDate>". date('d/m/Y à H:i') ."</lastBuildDate>
						<link>https://l3m.in</link>";


		$projects = new ProjectManager(null, null, null, null, null, null, null, null);
		$projects = $projects->getProjectsRss();

		foreach ($projects as $project) {
			$post_short_content = str_replace(['<', '>', '&',], ['&lt;', '&gt;', '&amp;',], $project['shortContent']);
			$contentProject .= "<item>
							<title>". $project['name'] ."</title>
							<description>". $post_short_content ."</description>
							<link>https://l3m.in/project/". $project['slug'] ."</link>
						</item>";
		}

		$contentProject .= "</channel></rss>";

		file_put_contents(RSSPROJECTS, $contentProject);
	}
}
