<?php
class ProjectManager extends Base
{
	private $name;
	private $link;
	private $content;
	private $shortContent;
	private $id;

	function __construct($name, $link, $cover, $src, $content, $shortContent, $id)
	{

		parent::__construct(); // like antibiotics
		$this->name = htmlspecialchars($name ?? '');
		$this->link = htmlspecialchars($link ?? '');
		$this->cover = $cover;
		$this->src = $src;
		$this->content = $content;
		$this->shortContent = $shortContent;
		$this->id = htmlspecialchars($id ?? '');
	}

	public function getProjects()
	{
		$req = self::$db->query('SELECT * FROM l3m_projects ORDER BY id DESC');

		return $req;
	}

	public function get($link)
	{
		$link = htmlspecialchars($link ?? '');

		$req = self::$db->prepare('SELECT * FROM l3m_projects WHERE slug = ?');
		$req->execute([$link]);
		$project = $req->fetch();

		if(!$project)
			return false;

		return $project;
	}

	public function getName($link){
		$link = htmlspecialchars($link ?? '');

		$req = self::$db->prepare('SELECT name FROM l3m_projects WHERE slug = ?');
		$req->execute([$link]);
		$name = $req->fetch();

		if(!$name)
			return false;
		return $name;
	}

    // todo vérifier qu'on poste pas un truc vide
	public function update()
	{
		$req = self::$db->prepare('UPDATE l3m_projects SET name = :name, link = :link, cover = :cover, src = :src, content = :content, shortContent = :shortContent WHERE id = :id');
		$req->execute([
			'name' => $this->name,
			'link' => $this->link,
			'cover' => $this->cover,
			'src' => $this->src,
			'content' => $this->content,
			'shortContent' => $this->shortContent,
			'id' => $this->id
		]);
	}

	public function getProjectsRss()
	{
		$req = self::$db->query('SELECT name, shortContent, slug FROM l3m_projects ORDER BY id DESC LIMIT 10');
		return $req;
	}

	public function getProjectsSitemap()
	{
		$req = self::$db->query('SELECT slug FROM l3m_projects ORDER BY id DESC');
		return $req;
	}

	public function send()
	{
		if(strlen($this->name) < 1)
		{
			header('Location:/patate');
			exit();
		}
		if(strlen($this->shortContent) < 1)
		{
			$this->shortContent = 'Pas de description.';
		}
		$this->slug = $this->slugify($this->name, "projects");

        $req = self::$db->prepare('INSERT INTO l3m_projects(name, slug, link, cover, src, content, shortcontent) VALUES(:name, :slug, :link, :cover, :src, :content, :shortContent)');
        $req->execute([
			'name' => $this->name,
			'slug' => $this->slug,
			'link' => $this->link,
			'cover' => $this->cover,
			'src' => $this->src,
			'content' => $this->content,
			'shortContent' => $this->shortContent
        ]);
	}

	public function delete($link)
	{
		$link = htmlspecialchars($link ?? '');

		$req = self::$db->prepare('DELETE FROM l3m_projects WHERE slug = ?');
		$req->execute([$link]);
	}
}
