<?php
class SitemapManager extends Base
{
	function updateSitemap()
	{
        // pages that will always exist
		$content = 
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
            <urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">
                <url>
                    <loc>https://l3m.in/</loc>
                </url>
                <url>
                    <loc>https://l3m.in/about</loc>
                </url>
                <url>
                    <loc>https://l3m.in/projects</loc>
                </url>
                <url>
                    <loc>https://l3m.in/contact</loc>
                </url>
                <url>
                    <loc>https://l3m.in/changelog</loc>
                </url>
                <url>
                    <loc>https://l3m.in/rss</loc>
                </url>
                <url>
                    <loc>https://l3m.in/rss/blog</loc>
                </url>
                <url>
                    <loc>https://l3m.in/rss/projects</loc>
                </url>";


		$blog = new BlogManager(null, null, null, null, null, null, null, null, null);
        $posts = $blog->getBlogSitemap();
		foreach ($posts as $post) {
			$content .= "<url>
                            <loc>https://l3m.in/article/". $post['slug'] ."</loc>
                        </url>\n";
        }

        $tag = new TagManager();
		$tags = $tag->getTagsSitemap();
		foreach ($tags as $tag) {
			$content .= "<url>
                            <loc>https://l3m.in/tag/". $tag['slug'] ."</loc>
                        </url>\n";
        }

		$projects = new ProjectManager(null, null, null, null, null, null, null, null);
		$projects = $projects->getProjectsSitemap();
		foreach ($projects as $project) {
			$content .= "<url>
                            <loc>https://l3m.in/project/". $project['slug'] ."</loc>
                        </url>\n";
        }

        $content .= "</urlset>";

		file_put_contents(SITEMAP, $content);
	}

}