<?php
// connexion à la bdd
class Base
{

	// we have a static connection to the db
	protected static $db;

	// we try to connect to db at each instantiation
	function __construct()
	{
		$this->dbConnect();
	}

	protected function dbConnect()
	{
		// verify if a connexion to the db already exist
		if (!(self::$db instanceof PDO))
			self::$db = new PDO('mysql:host='. dbAddr .';dbname='. dbName .';charset=utf8mb4', dbUser, dbPass);
	}


	// thanks https://www.matthecat.com/supprimer-les-accents-dune-chaine-en-php/ & http://php.net/manual/fr/transliterator.transliterate.php#110598
	protected function slugify($text, $table)
	{
		$slug = preg_replace('/&amp;/', 'et', $text);
		$slug = transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();", $slug);
		$slug = preg_replace('/[-\s]+/', '-', $slug);
		$slug = preg_replace('/-$/', '', $slug);
		$slug = preg_replace('/[^a-z0-9-]+/', '', $slug);

		if($slug == "")
			$slug = "default";

		// check if the slug already exist
		$i = 1;
		$old = $slug;
		do{
			$req = self::$db->prepare('SELECT slug FROM l3m_'. $table .' WHERE slug = ?');
			$req->execute([$slug]);
			$slugExist = $req->fetch();
			if($slugExist)
			{
				$i++;
				$slug = $old . "-" . $i;
			}
		}
		while($slugExist);

		return $slug;
	}

}
