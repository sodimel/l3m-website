<?php
class BlogManager extends Base
{
	private $time;
	private $title;
	private $content;
	private $shortContent;
	private $id;
	private $tags;

	function __construct($time, $title, $cover, $alt, $content, $shortContent, $id, $tags)
	{
		parent::__construct(); // like antibiotics
		$this->time = htmlspecialchars($time ?? '');
		$this->title = htmlspecialchars($title ?? '');
		$this->cover = $cover;
		$this->content = $content;
		$this->shortContent = $shortContent;
		$this->id = htmlspecialchars($id ?? '');
		$this->tags = htmlspecialchars($tags ?? '');
	}

	public function getPage($page)
	{
		$start = ($page-1)*5;
		$req = self::$db->query('SELECT * FROM l3m_blog ORDER BY id DESC LIMIT '. $start .', 5');

		// si on tente d'accéder à une page qui ne comporte aucun article (genre la page 2 lol)
		// la blague du commentaire précédent vous est présentée par "le site de dev qui comporte 4 posts tout au plus"
		// blague obsolète lol
		if($req->rowCount() == 0 && $page != 1)
			header('Location:/');

		return $req;
	}

	public function get($link)
	{
		$link = htmlspecialchars($link ?? '');

		$req = self::$db->prepare('SELECT * FROM l3m_blog WHERE slug = ?');
		$req->execute([$link]);
		$post = $req->fetch();

		if(!$post)
			return false;

		$post['datetime'] = new DateTime($post['time']);

		$tag = new TagManager();
		$post['tags'] = $tag->getTags($post['id']);

		return $post;
	}

	public function getTitle($link){
		$link = htmlspecialchars($link ?? '');

		$req = self::$db->prepare('SELECT title FROM l3m_blog WHERE slug = ?');
		$req->execute([$link]);
		$title = $req->fetch();

		if(!$title)
			return false;
		return $title;
	}

	public function getall()
	{
		$req = self::$db->query('SELECT * FROM l3m_blog ORDER BY id DESC');
		return $req;
	}

	public function getTag($tag)
	{
		$req = self::$db->prepare('SELECT l3m_blog.*, l3m_tag.tag FROM l3m_blog, l3m_tag_post, l3m_tag WHERE l3m_blog.id = l3m_tag_post.post AND l3m_tag.id = l3m_tag_post.tag AND l3m_tag.slug = ? ORDER BY id DESC');
		$req->execute([$tag]);

		$posts = $req->fetchAll();

		if(!$posts)
			return false;

		return $posts;
	}

	public function getBlogRss()
	{
		$req = self::$db->query('SELECT time, title, shortContent, slug FROM l3m_blog ORDER BY id DESC LIMIT 10');
		return $req;
	}

	public function getBlogSitemap()
	{
		$req = self::$db->query('SELECT slug FROM l3m_blog ORDER BY id DESC');
		return $req;
	}

	public function pagingList($current)
	{
		$total = self::$db->query('select count(*) from l3m_blog')->fetchColumn();

		$after = [];
		$before = [];
		$nbPages = ceil($total/5);
		$first = false;
		$last = false;

		$limit = 2; // afficher deux pages avant & deux pages après

		// on part d'un peu avant current, on va jusqu'à current
		for($i = $current-$limit; $i < $current; $i++)
			// si on est pas sur une page négative
			if($i > 0)
				// on ajoute cette page à la liste des pages avant
				array_push($before, $i);

		// on part de la page suivant current, et on va jusqu'à nbPages+1
		for($i = $current+1; $i < $nbPages+1; $i++)
			// si on dépasse pas trop
			if($i < $current+$limit+1)
				// on ajout cette page à la liste des pages après
				array_push($after, $i);

		// si la page courante est loin de la première page, on active le lien sur la flêche gauche
		if($current-$limit > 1)
			$first = true;

		// si la page courante est loin de la dernière page, on active le lien sur la flêche droite
		if($current+$limit < $nbPages)
			$last = $nbPages;

		return array('after' => $after, 'before' => $before, 'current' => $current, 'first' => $first, 'last' => $last);
	}

	// todo vérifier qu'on poste pas un truc vide
	public function update()
	{
		$req = self::$db->prepare('UPDATE l3m_blog SET time = :time, title = :title, cover = :cover, content = :content, shortContent = :shortContent WHERE id = :id');
		$req->execute([
			'time' => $this->time,
			'title' => $this->title,
			'cover' => $this->cover,
			'content' => $this->content,
			'shortContent' => $this->shortContent,
			'id' => $this->id
		]);

		$tag = new TagManager();
		// on enlève les vieux tags
		$tag->deleteTag($this->id);

		// on ajoute les nouveaux
		$tag->addTag($this->tags, $this->id);
	}

	// envoi d'un post de blog
	public function send()
	{
		if(strlen($this->time) < 1)
		{
			$this->time = new datetime();
			$this->time = $this->time->format('Y-m-d H:i:s');
		}
		if(strlen($this->title) < 1)
		{
			header('Location:/patate');
			exit();
		}
		if(strlen($this->shortContent) < 1)
			$this->shortContent = 'Ce billet de blog ne dispose d\'aucune description.';

		$this->slug = $this->slugify($this->title, "blog");

		$req = self::$db->prepare('INSERT INTO l3m_blog(time, title, cover, slug, content, shortContent) VALUES(:time, :title, :cover, :slug, :content, :shortContent)');
		$req->execute([
			'time' => $this->time,
			'title' => $this->title,
			'cover' => $this->cover,
			'slug' => $this->slug,
			'content' => $this->content,
			'shortContent' => $this->shortContent
		]);

		$tag = new TagManager();
		// on ajoute les nouveaux tags
		$tag->addTag($this->tags, self::$db->lastInsertId());
	}

	// suppression d'un post de blog
	public function delete($link)
	{
		$link = htmlspecialchars($link ?? '');

		$req = self::$db->prepare('SELECT id FROM l3m_blog WHERE slug = ?');
		$req->execute([$link]);
		$id = $req->fetch();

		$req = self::$db->prepare('DELETE FROM l3m_blog WHERE slug = ?');
		$req->execute([$link]);

		$tag = new TagManager();
		// on n'oublie pas d'enlever les tags
		$tag->deleteTag($id['id']);
	}
}
