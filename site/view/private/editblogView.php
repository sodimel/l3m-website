<?php
	if(!isset($admin)){
		header('Location:/patate');
		exit();
	}

	$tags = "";
	$space = false;
	if(isset($blogpost['tags']) && $blogpost['tags']){
		foreach ($blogpost['tags'] as $tag){
			if($space)
				$tags .= ", ";
			else
				$space = true;
			$tags .= $tag["tag"];
		}
	}
?>

<article>
	<h3>Édition du billet de blog #<?php echo $blogpost['id']; ?></h3>

	<form action="/patate/blog/<?php echo $blogpost['slug']; ?>/edit" method="post" accept-charset="utf-8">

		<input type="hidden" name="id" value="<?php echo $blogpost['id']; ?>"  />

		<article class="input">
			<label for="inputTitle">Titre</label>
			<input type="text" name="title" value="<?php echo $blogpost['title']; ?>" id="inputTitle" />
		</article>

		<section class="input">
			<label for="inputCoverURL">Cover:</label>
			<input type="url" name="cover" placeholder="Lien" value="<?php echo $blogpost['cover']; ?>" id="inputCoverURL" />
			<p class="small">
				<i>Attention ; les images d'en-tête doivent faire 800x305px (600x305px au centre affichés sur tous les écrans) et être <b>vraiment</b> bien optimisées.</i>
			</p>
		</section>

		<section class="input">
			<label for="inputCoverSrc">Src de la cover:</label>
			<input type="text" name="src" placeholder="Description de la cover" value="<?php echo $blogpost['src']; ?>" id="inputCoverSrc" />
		</section>

		<article class="input">
			<label for="inputDate">Date de publication</label>
			<input type="datetime-local" name="time" value="<?php echo $blogpost['datetime']->format('Y-m-d\TH:i:s'); ?>" class="small" id="inputDate" />
		</article>

		<article class="input">
			<label for="inputShortContent">Description</label>
			<textarea name="shortContent" id="inputShortContent"><?php echo $blogpost['shortContent']; ?></textarea>
		</article>

		<section class="input">
			<label for="inputTags">Tags</label>
			<textarea name="tags" id="inputTags"><?php echo $tags; ?></textarea>
		</section>

		<article class="input">
			<label for="inputContent">Contenu</label>
			<textarea name="content" id="inputContent"><?php echo $blogpost['content']; ?></textarea>
		</article>

		<input type="submit" value="Éditer" />

	</form>
</article>