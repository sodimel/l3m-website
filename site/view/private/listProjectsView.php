<?php
	if(!isset($admin)){
		header('Location:/patate');
		exit();
	}

	foreach ($projects as $project){

		$exist = true;
		?>
		<article>
			<p class="listAdmin">
				<b><a href="/project/<?php echo $project['slug']; ?>"><?php echo $project['name'] ?></a></b> 
				<span class="postInfos"><?php echo $project['link']; ?></span> <a class="button" href="/patate/project/<?php echo $project['slug'] ."/edit"; ?>">📝</a> <a class="button" href="/patate/project/<?php echo $project['slug'] ."/delete"; ?>" onclick="return confirm('C\'est certain ?')">❌</a>
			</p>
		</article>
		<?php
	}
	if(!isset($exist)){
		?>
		<article>
			<h4>Pas de projet</h4>

			<p>Aucun projet n'a encore été publié, patience !</p>
		</article>
		<?php
	}
