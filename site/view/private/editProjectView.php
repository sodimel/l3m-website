<?php
	if(!isset($admin)){
		header('Location:/patate');
		exit();
	}
?>

<article>
	<h3>Édition du projet #<?php echo $project['id']; ?></h3>

	<form action="/patate/project/<?php echo $project['slug'] ?>/edit" method="post" accept-charset="utf-8">

		<input type="hidden" name="id" value="<?php echo $project['id']; ?>"  />

		<article class="input">
			<label for="inputTitle">Nom du projet</label>
			<input type="text" name="name" value="<?php echo $project['name']; ?>" id="inputName" />
		</article>

		<article class="input">
			<label for="inputLink">Lien vers le projet</label>
			<input type="url" name="link" value="<?php echo $project['link']; ?>" id="inputLink" />
		</article>

		<section class="input">
			<label for="inputCoverURL">Cover:</label>
			<input type="url" name="cover" placeholder="Lien" value="<?php echo $project['cover']; ?>" id="inputCoverURL" />
			<p class="small">
				<i>Attention ; les images d'en-tête doivent faire 800x305px (600x305px au centre affichés sur tous les écrans) et être <b>vraiment</b> bien optimisées.</i>
			</p>
		</section>

		<section class="input">
			<label for="inputCoverSrc">Src de la cover:</label>
			<input type="text" name="src" placeholder="Description de la cover" value="<?php echo $project['src']; ?>" id="inputCoverSrc" />
		</section>

		<article class="input">
			<label for="inputShortContent">Résumé</label>
			<textarea name="shortContent" id="inputShortContent"><?php echo $project['shortContent']; ?></textarea>
		</article>

		<section class="input">
			<label for="inputContent">Description</label>
			<textarea name="content" id="inputContent"><?php echo $project['content']; ?></textarea>
		</section>

		<input type="submit" value="Éditer" />

	</form>
</article>