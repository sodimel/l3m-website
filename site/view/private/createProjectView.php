<?php
	if(!isset($admin)){
		header('Location:/patate');
		exit();
	}

	$date = new datetime();
?>

<article>
	<h3>Post d'un nouveau projet</h3>

	<form action="/patate/project/submit" method="post" accept-charset="utf-8">

		<section class="input">
			<label for="inputName">Nom du projet</label>
			<input type="text" name="name" placeholder="Nom du projet" id="inputName" />
		</section>

		<section class="input">
			<label for="inputLink">Lien vers le projet</label>
			<input type="url" name="link" placeholder="Lien vers le projet" id="inputLink" />
		</section>

		<section class="input">
			<label for="inputCoverURL">Cover:</label>
			<input type="url" name="cover" placeholder="Lien" id="inputCoverURL" />
			<p class="small">
				<i>Attention ; les images d'en-tête doivent faire 800x305px (600x305px au centre affichés sur tous les écrans) et être <b>vraiment</b> bien optimisées.</i>
			</p>
		</section>

		<section class="input">
			<label for="inputCoverSrc">Source de la cover:</label>
			<input type="text" name="src" placeholder="Description de la cover" id="inputCoverSrc" />
		</section>

		<section class="input">
			<label for="inputShortContent">Résumé</label>
			<textarea name="shortContent" id="inputShortContent"></textarea>
		</section>

		<section class="input">
			<label for="inputContent">Description</label>
			<textarea name="content" id="inputContent"></textarea>
		</section>

		<input type="submit" value="Envoyer" />

	</form>
</article>
