<?php
	if(!isset($admin)){
		header('Location:/patate');
		exit();
	}
?>

<article>
	<h3>Connexion à la partie administration du site</h3>
	
	<p>Attention au ban ip à partir de deux erreurs !</p>

	<form action="/patate" method="post" accept-charset="utf-8">
		<input type="text" name="user" placeholder="Pseudonyme" autofocus />
		<input type="password" name="pass" placeholder="Mot de passe" />
		
		<input type="submit" value="Connexion" />

	</form>
</article>