<?php
	if(!isset($admin)){
		header("Location:/patate");
		exit();
	}

	foreach ($blogposts as $post){
		$post['datetime'] = new DateTime($post['time']);		
		$exist = true;
		?>
		<article>
			<p class="listAdmin">
				<b><a href="/article/<?php echo $post['slug']; ?>"><?php echo $post['title'] ?></a></b> 
				<span class="postInfos"><?php echo date_format($post['datetime'], 'd/m/Y à H:i'); ?></span> <a class="button" href="/patate/blog/<?php echo $post['slug']; ?>/edit">📝</a> <a class="button" href="/patate/blog/<?php echo $post['slug'] ."/delete"; ?>" onclick="return confirm('C\'est certain ?')">❌</a>
			</p>
		</article>
		<?php
	}
	if(!isset($exist)){
		?>
		<article>
			<h4>Pas de post</h4>

			<p>Aucun billet n'a encore été publié, patience !</p>
		</article>
		<?php
	}
