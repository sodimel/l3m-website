<?php
	if(!isset($admin)){
		header('Location:/patate');
		exit();
	}
?>

<article>
	<h3>🔧 Administration</h3>
	<p>Ici c'est pour créer, éditer ou supprimer des billets de blog ou des projets.</p>
</article>

<article>
	<section id="listAdmin">
		<article>
			<h3>Liste des posts de blog</h3>

			<form action="/patate/blog/submit" method="get" accept-charset="utf-8">
				<input type="submit" value="Nouveau billet" />
			</form>

			<?php include('site/view/private/listBlogPostView.php'); ?>

		</article>

		<article>
			<h3>Liste des projets</h3>

			<form action="/patate/project/submit" method="get" accept-charset="utf-8">
				<input type="submit" value="Nouveau projet" />
			</form>

			
			<?php include('site/view/private/listProjectsView.php'); ?>
			
		</article>
	</section>
</article>