<?php
	if(!isset($admin)){
		header('Location:/patate');
		exit();
	}

	$date = new datetime();
?>

<article>
	<h3>Post d'un billet de blog</h3>

	<form action="/patate/blog/submit" method="post" accept-charset="utf-8">

		<section class="input">
			<label for="inputTitle">Titre</label>
			<input type="text" name="title" placeholder="Titre" id="inputTitle" autofocus required />
		</section>

		<section class="input">
			<label for="inputCoverURL">Cover:</label>
			<input type="url" name="cover" placeholder="Lien" id="inputCoverURL" />
			<p class="small">
				<i>Attention ; les images d'en-tête doivent faire 800x305px (600x305px au centre affichés sur tous les écrans) et être <b>vraiment</b> bien optimisées.</i>
			</p>
		</section>

		<section class="input">
			<label for="inputCoverSrc">Source de la cover:</label>
			<input type="text" name="src" placeholder="Description de la cover" id="inputCoverSrc" />
		</section>

		<section class="input">
			<label for="inputDate">Date de publication</label>
			<input type="datetime-local" name="time" placeholder="<?php echo $date->format("Y-m-d\TH:i:s"); ?>" class="small" id="inputDate" />
		</section>

		<section class="input">
			<label for="inputShortContent">Description</label>
			<textarea name="shortContent" id="inputShortContent"></textarea>
		</section>

		<section class="input">
			<label for="inputTags">Tags</label>
			<textarea name="tags" id="inputTags"></textarea>
		</section>

		<section class="input">
			<label for="inputContent">Contenu</label>
			<textarea name="content" id="inputContent"></textarea>
		</section>

		<input type="submit" value="Envoyer" />
	</form>
</article>