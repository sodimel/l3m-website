	<article>
		<h3 id="aboutme">À propos de moi</h3>
		<p>
			Je suis Corentin Bettiol, et je travaille en tant que développeur web chez <a href="https://kapt.mobi">Kapt</a>.
		</p>
		<p>
			J'ai suivi une <a href="https://univ-avignon.fr/rechercher-une-formation/licence-informatique-1255.kjsp">Licence informatique</a> à la faculté d'Avignon, puis un <a href="https://master-informatique.univ-grenoble-alpes.fr/">M1 informatique</a> à l'université de Grenoble, et j'ai enfin réalisé une licence professionnelle à distance en alternance à la faculté de Strasbourg (la <a href="https://lpdwca.eformation-webmaster.net/">LPDWCA</a>).
		</p>
		<p>
			À côté du boulot j'aime bien l'escalade, la photo, la randonnée, le dessin, la lecture, le web...
		</p>
	</article>

	<article>
		<h3 id="aboutwebsite">À propos du site</h3>
		<p>
			Ce site web est le fruit de plusieurs années de déambulations sur le web.
		</p>
		<p>
			Il me sert de point de centralisation de plusieurs de mes projets, et me permet de faire quelques tests pas forcément référencés.
		</p>
		<p>
			Il en est à sa cinquième refonte, et son code source est disponible <a href="https://git.bitmycode.com/sodimel/l3m-website">ici</a> sous la licence <a href="http://www.wtfpl.net/">WTFPL</a>.
		</p>
	</article>

	<article>
		<h3 id="aboutcontent">À propos du contenu</h3>
		<p>
			Sauf mention contraire, tout le contenu que je poste sur ce site (photos, images, et textes entre autres choses) est sous licence <a href="https://creativecommons.org/licenses/by/3.0/fr/">CC BY 3.0 FR</a> (attribution).
		</p>
		<p>
			Il est possible que je cite du contenu disponible sur d'autres sites et que je le copie sur mon serveur pour éviter que certains liens puissent se tranformer en références mortes.
		</p>
		<p>
			Si une image d'en-tête ne m'appartient pas, la source est indiquée à côté des tags.
		</p>
		<p>
			Si vous trouvez du contenu sur ce site vous appartenant et que vous voulez que je le retire, merci de me contacter à l'adresse suivante : corentin, signe arobase, <a href="https://244466666.xyz">ce nom de domaine</a>.
		</p>
		<p>
			Merci de ne pas effectuer de démarchage sur cette adresse mail.
		</p>
	</article>

	<article>
		<h3 id="aboutserver">À propos du serveur</h3>
		<p>
			<i>nb: cette partie n'est plus exacte depuis le 06/03/2020, cf le post <a href="http://l3m.in/article/il-est-vivant">Il est vivant !</a></i><br />
			<i><b>update :</b> cette partie est de nouveau exacte depuis le 25/11/2020, cf le post <a href="https://l3m.in/article/demenagement-du-site">Déménagement du site</a>.</i>
		</p>
		<p>
			Le serveur est en fait un dell optiplex fx160 (acheté 50€ en 2017 sur ebay) sous debian, et actuellement posé à côté de ma box internet.
		</p>
		<p class="aligncenter">
			<img src="img/serveur_petit.png" alt="image du serveur" />
		</p>
		<p class="aligncenter">
			<i>Photo du serveur prise le 02/03/2019, après la grosse réinstallation. C'est le petit machin à droite.</i>
		</p>
	</article>

	<article>
		<h3 id="aboutlogo">À propos du logo</h3>
		<p>
			C'est un morse. C'est le pixel-art résultant d'un petit dessin gribouillé dans une marge d'une feuille au lycée.
		</p>
	</article>

	<article>
		<h3 id="aboutreadingtime">À propos du temps de lecture</h3>
		<p>
			Les articles du blog et les projets ont un temps de lecture affiché au dessous du titre.<br />
			Ce temps est obtenu en arrondissant à l'entier inférieur le résultat de [nombre de mots]/249.
		</p>
		<p>
			249 n'est pas un nombre sorti de nulle part. Il s'agit du nombre de mots lus par minutes en français <i>(<a href="https://iovs.arvojournals.org/article.aspx?articleid=2166061">Standardized Assessment of Reading Performance: The New International Reading Speed Texts</a> &mdash; Investigative Ophthalmology & Visual Science August 2012, Vol.53, 5452-5461)</i>.
		</p>
	</article>

	<article>
		<h3 id="aboutstats">À propos des visites sur ce site</h3>
		<p>
			Je reçois <b>beaucoup</b> plus de visites de bots que de visiteurs humains. Sur tous les sites & sous-domaines que j'ai créé. Du coup je me suis demandé ce que ça donnerait d'avoir quelques stats concernant les visites. J'avais déjà trouvé GoAccess en déambulant sur le net, puis j'ai appris qu'il pouvait créer un fichier html et l'incrémenter avec des données live avec des websockets. Du coup jme suis dit que j'allais lancer un service qui démarre auto et qui crée un index.html sur un nouveau sous-domaine.
		</p>
		<p>
			J'ai lancé un service de <a href="https://goaccess.io">GoAccess</a> sur <a href="https://stats.l3m.in/">https://stats.l3m.in/</a>, qui montre bien le nombre de robots (j'ai anonymisé les IP de vous, les visiteurs, mais toutes les autres infos sont dispo telles quelles). Les stats sont issues de tous les sites que j'ai, à l'exception de mon instance Nextcloud.
		</p>
	</article>

	<article>
		<h3 id="aboutfriends">À propos des amis</h3>
		<p>
			Voici quelques amis de l'internet, pêle-mêle :
		</p>
		<ul>
			<li><a href="https://yannicka.fr/">Yannicka</a>, le site d'un ami du web, devenu un ami pour de vrai dans la vraie vie véritable.</li>
			<li><a href="https://ctrlist.org/">ctrlist</a>, des copains de l'internet.</li>
			<li><a href="http://froggames.free.fr/">Frogg</a>, le créateur de ce site de ptits jeux est sympa, il a utilisé mes vieilles tiles de AutoRPG sur son jeu Fight Magic :)</li>
			<li><a href="https://tomoe.hotglue.me/">tomoe</a>, le site d'un ami de l'internet un peu perdu de vue. Des dessins bizarres, des musiques cheloues.</li>
			<li><a href="https://eternaltwin.org/">Eternal Twin</a>, le site web des successeurs des sites de jeux de la Motion Twin.</li>
		</ul>
	</article>
