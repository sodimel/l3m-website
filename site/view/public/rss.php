	<article>
		<h3><a href="/rss/blog" title="flux rss du blog">Flux RSS du blog</a></h3>

		<p>
			Ce flux RSS vous permet d'avoir accès aux dix derniers messages du blog postés.
		</p>
	</article>

	<article>
		<h3><a href="/rss/projects" title="flux rss des projets">Flux RSS des projets</a></h3>

		<p>
			Ce flux RSS vous permet d'avoir accès aux dix derniers projets créés.
		</p>
	</article>
