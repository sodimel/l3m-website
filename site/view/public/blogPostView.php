<?php
	// si on essaye de trafiquer l'url
	if(!isset($pageName))
	{
		header('Location:/');
		exit();
	}

	require_once('site/libs/Parsedown.php');
	require_once('site/libs/ParsedownExtra.php');
	$parsed = new ParsedownExtra();

	// liste des tags
	$tags = "";
	if(isset($blogpost['tags']) && $blogpost['tags'])
	{
		foreach ($blogpost['tags'] as $tag) {
			$tags .= "<em class=\"tag\"><a href=\"/tag/". $tag["slug"] ."\">". $tag["tag"]."</a></em>";
		}
	}
	else
		$tags .= "<em>Aucun tag</em>";

	// thx https://iovs.arvojournals.org/article.aspx?articleid=2166061
	// french = 249 wpm
	$nbwords = str_word_count($blogpost['content']);
	$readingtime = floor($nbwords/249);
	if($readingtime < 2)
		$readingtime = "moins de 2";
?>

	<article <?php if($blogpost['cover']) { echo 'class="covercontainer"'; } else { echo 'class="simple"'; } ?>>
		<section <?php if($blogpost['cover']) { echo 'class="cover"'; } ?>>
			<?php if($blogpost['cover']) { ?>
				<img src="<?php echo $blogpost['cover']; ?>" class="coverImg" alt="cover for <?php echo $blogpost['title']; ?>" loading="lazy" />
			<?php } ?>

			<h3><a href="/article/<?php echo $blogpost['slug']; ?>"><?php echo $blogpost['title']; ?></a></h3>
			<p class="postInfos">
				Le <time datetime="<?php echo $blogpost['time']; ?>"><?php echo date_format($blogpost['datetime'], 'd/m/Y à H:i'); ?></time> &mdash; 📖 <?php echo $readingtime; ?> min &mdash; <?php echo $tags; ?>
			<?php if(isset($_GET['action']) && $_GET['action'] == 'blog' && !isset($_GET['page']) && $blogpost['src']){ ?>
				<a href="<?php echo $blogpost['src']; ?>">Source de l'image d'en-tête</a>
			<?php } ?>
			</p>
		</section>
		<section>
				<?php
				// si on est sur un post de blog (et pas sur une page listant des posts de blog) on affiche l'article
				if(isset($_GET['action']) && $_GET['action'] == 'blog' && !isset($_GET['page'])){
					if (date_format(date_create($blogpost['time']), "Y") > 2021)
						echo $parsed->text(html_entity_decode($blogpost['content']));
					else
						echo htmlspecialchars_decode($blogpost['content']);
				// sinon si on est sur l'accueil ou sur une page listant des posts de blog on affiche le résumé
				} else
					echo $parsed->text(html_entity_decode($blogpost['shortContent']));
				?>
		</section>
	</article>
