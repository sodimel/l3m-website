<article>
	<h3>14/01/2023</h3>
	<ul>
		<li>Fix d'un bug bien relou suite à des modifs sur le champ offset. Il était encore présent en base mais le fait de l'ajouter dans la requête faisait tout planter. J'aurais du faire un site Django avec des modèles et des migrations :(</li>
		<li>Màj page de contact (simplification du mail).<li>
		<li>Ajout d'images de previews rézosociaux sur les articles de blog & les pages détail des projets.</li>
	</ul>
</article>


<article>
	<h3>09/07/2022</h3>
	<ul>
		<li>Mise à jour de Parsedown et ajout de ParsedownExtra, qui me permet de rajouter simplement des ancres dans le texte, et du coup de rajouter aussi des liens vers ces ancres.</li>
	</ul>
</article>


<article>
	<h3>18/01/2022</h3>
	<ul>
		<li>Passage au markdown pour la rédaction du contenu, affichage via la lib <a href="https://github.com/erusev/parsedown">Parsedown</a>.</li>
		<li><b>À partir de maintenant (depuis plusieurs (dizaines) de mois en fait), je ne pense plus éditer ce fichier. Les logs (moches) restent consultables sur <a href="https://git.bitmycode.com/sodimel/l3m-website/-/commits/master/">le repo</a>.</b></li>
	</ul>
</article>


<article>
	<h3>XX/XX/2021</h3>
	<ul>
		<li>Quelques petites màj d'optimisation :
			<ul>
				<li>MAINT: update projects : add external project link in post infos, replace link in project title</li>
				<li>MAINT: replace cover bg by img tag & add lazy load</li>
				<li>FEAT: wysiwyg buttons are now sticky</li>
				<li>MAINT: update robots.txt to disallow jetsli.de crawler</li>
			</ul>
		</li>
	</ul>
</article>


<article>
	<h3>22/05/2021</h3>
	<ul>
		<li>Nouvel effet de hover sur les titres des articles.</li>
		<li>Les images de cover ont maintenant une hauteur fixe et s'affichent mieux sur petits écrans (la hauteur était variable avant).</li>
		<li>Le souci de border-bottom sur les liens du menu est fixé !</li>
	</ul>
</article>


<article>
	<h3>18/01/2021</h3>
	<ul>
		<li>Alors que je corrige une typo, voilà que je remarque que le site est également présent sur <a href="https://512kb.club/">512kb.club</a>, du coup je le note ici.</li>
	</ul>
</article>


<article>
	<h3>02/01/2021</h3>
	<ul>
		<li>Optimisation de icon.png, mobile-menu-icon & mobile-menu-icon-close.png, et de serveur_petit.png</li>
		<li>Suppression de ubuntu light, remplacement de ubuntu mono par un subset de Share Tech Mono (Open Font License). Je tiens également à signaler ici que mon site est présent sur <a href="https://1mb.club/">1mb.club</a>!</li>
	</ul>
</article>

<article>
	<h3>31/12/2020</h3>
	<p>Et voilà un autre batch de mises à jour effectuées depuis mai 2020 jusqu'à aujourd'hui (toujours en vrac et sans les liens) :</p>
	<ul>
		<li>Optimisations des différentes images du site (mais genre des optimisations vénères)</li>
		<li>Modif des images d'en-tête (normalement plus besoin d'offset)</li>
		<li>Mises à jour de plein de design (blog, section admin, menu...)</li>
		<li>Màj du contenu de la balise meta title pour les projets</li>
		<li>Màj du contenu des pages à propos & contact</li>
		<li>Ajout du numéro de la page dans le titre des pages du blog (pour le SEO)</li>
		<li>Màj du contenu de la balise description sur le blog (pour le SEO)</li>
		<li>Ajout d'un fichier robots.txt</li>
	</ul>
</article>

<article>
	<h3>10/05/2020</h3>
	<p>
		Rien n'est automatisé sur cette page, et j'avoue avoir un peu oublié de noter les différents changements depuis quelques mois.<br />
		Alors un peu en vrac et sans les liens vers les commits respectifs, on a eu :
	</p>
	<ul>
		<li>Mise à jour de quelques trucs en css</li>
		<li>Changement des polices d'écritures (passage de ttf à woff2, beaucoup plus léger)</li>
		<li>Ajout du temps de lecture sur chaque article & projet (idée via <a href="https://dev.to/robole/add-the-estimated-reading-time-to-your-blog-do-you-read-code-at-275-words-per-minute-3don">ce post</a> sur dev.to)</li>
		<li>Ajout d'images d'en-tête sur les posts du blog & des projets (idée via la réalisation d'un <a href="http://web.archive.org/web/20200510154825/https://wordpress.l3m.in/">portfolio</a> wordpress pour les cours &mdash; <a href="https://up.l3m.in/file/1589126382-screenshot-wordpress.jpg">screenshot de toutes les pages</a>)</li>
		<li>Réparation des flux RSS pour le <a href="https://l3m.in/rss/blog">blog</a> & les <a href="https://l3m.in/rss/projects">projets</a></li>
		<li>Création d'un <a href="https://l3m.in/sitemap.xml">sitemap</a> (pour les gentils crawlers)</li>
		<li>Modification de la meta description des tags, pour éviter d'avoir des pages "dupliquées".</li>
		<li>Mise à jour des pages à propos & contact.</li>
		<li>Minification du logo.</li>
	</ul>
	<p>
		<i>nb: vu que le site est open source, vous pouvez trouver une liste des commits <a href="https://git.bitmycode.com/sodimel/l3m-website/-/commits/master">ici</a> :)</i>
	</p>
</article>

<article>
	<h3>22/03/20</h3>
	<ul>
		<li>Mise à jour du title des pages</li>
		<li>Correction du label de création de projet</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/-/commit/01c3961e518c1778ce4e4e38a5f018a2015cd49f">06/03/20</a></h3>
	<ul>
		<li>Ajout d'une variable pour la connexion à la bdd (passage de l'autohébergé à alwaysdata).</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/-/commit/30442b34f8a8ece526af53a9c451adfe529a5dff">03/10/19</a></h3>
	<ul>
		<li>Mise à jour de la page "à propos".</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/71c01a129e121713cd9f7fb254801a4f07117672">10/04/19</a></h3>
	<ul>
		<li>Minification du css (le css lisible reste disponible <a href="/css/design.css">ici</a>)</li>
		<li>Activation du cache pour les polices d'écriture, mise à jour du fichier de config apache</li>
		<li>Changement des flèches de pagination (pour éviter l'affichage sous forme d'emoji), transformation de l'id "paging" en class pour pouvoir l'utiliser deux fois</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/8d6ca2c5fa280e4da2d2700146243c451b7416ec">09/04/19</a></h3>
	<ul>
		<li>Changement de largeur du texte des billets de blog/des projets (petit écran & grand écran)</li>
		<li>Léger changement de couleur pour les appareils avec un mauvais contraste</li>
		<li>Ajout de la licence WTFPL au code source du site</li>
		<li>Ajout de la licence CC BY 3.0 FR au contenu du site</li>
		<li>Modifications de la page À Propos</li>
		<li>Suppression du contenu des fichiers rss</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/b88795be2f8bb0c58cabe5d2ffeb0da86b163bc1">03/04/19</a> - "release"</h3>
	<ul>
		<li>Correction de l'affichage de la description de l'article ou de son contenu selon que l'on est sur la liste des articles/tags ou sur la page de l'article</li>
		<li>Affichage du texte un peu plus petit pour les petits écrans (< 1500px de large)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/89a7796a6abd813c07b44c2273cd2f91e0299a4c">21/03/19</a></h3>
	<ul>
		<li>Ajout d'un slug pour les tags + possibilité de stocker des tags "emoji" dans la bdd</li>
		<li>Ajout d'un effet de hover sur les tags</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/72e337a3db699bac92f4e25828a8c0b4dee47bd6">20/03/19</a></h3>
	<ul>
		<li>Création des tags :
			<ul>
				<li>Classe TagManager</li>
				<li>Fonctions pour ajouter/supprimer des tags</li>
				<li>Tables l3m_tag & l3m_tag_post pour lier les tags aux posts</li>
				<li>Vue avec tous les messages comprenant un tag particulier</li>
			</ul>
		</li>
		<li>Mise à jour de la synthaxe d'écriture des conditions/fonctions (ajout d'un retour à la ligne)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/a2b96bcc34b51c103691a6a2aafb06e05ce421ce">19/03/19</a></h3>
	<ul>
		<li>Création des flux rss :
			<ul>
				<li>Classe RssManager</li>
				<li>Fonctions updateRssBlog() & updateRssProjects()</li>
				<li>Appel de ces fonctions lors de chaque ajout/edit/suppression de billet de blog/projet</li>
			</ul>
		</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/f69c0c7d8f392f762ea706f11a503fbfc3d352d6">15/03/19</a></h3>
	<ul>
		<li>Màj du padding du menu pour la version tablette</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/d4ee13c79b8bc2c43da23677f6c4d4d015700b69">12/03/19</a></h3>
	<ul>
		<li>Mise à jour de l'éditeur wysiwyg</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/d1d9e3e6c07bf37bf963bd76a8e66a0d193dd924">06/03/19</a></h3>
	<ul>
		<li>Changement de charset (utf8 -> utf8mb4_unicode_ci) ! Les emojis sont désormais stockés dans la bdd</li>
		<li>Ajout d'une div avec un tag "contenteditable", pour éditer facilement des paragraphes/titres/... dans le contenu des posts de blog</li>
		<li>Ajout d'une liste de boutons pour modifier du texte en wysiwyg</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/dc8dcc3e7e53f487b3360c21407a743d1020c1f7">05/03/19</a> (encore)</h3>
	<ul>
		<li>Bugfix sur l'opacité de la pagination (qui apparaissait par dessus le menu)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/63524a574490e130da7fee8076b8d2af047da58f">05/03/19</a></h3>
	<ul>
		<li>Ajout de la pagination sur le blog :
			<ul>
				<li>Fonctions pagingList dans BlogManager</li>
				<li>Fichier blogPagingView.php dans /site/view/public</li>
			</ul>
		</li>
		<li>Mise à jour de l'affichage du menu pour la version tablette du site</li>
		<li>Réorganisation de l'affichage des pages par le BlogManager</li>
		<li>Ajout d'un saut de ligne au footer (pour éviter un retour à la ligne automatique disgratieux sur mobile)</li>
		<li>Activation du retour à la ligne pour les longues chaines de caractères (url git par exemple)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/49c5a5f497056359c55fc93d1b35f0a3bdd25b44">03/03/19</a></h3>
	<ul>
		<li>Màj menu hamburger ; il affiche la page courante maintenant</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/6088891e4bf5a032a2746d6d3ad2d326b304704b">02/03/19</a></h3>
	<ul>
		<li>Ajout du menu hamburger pour les smartphones</li>
		<li>Ajout du renommage automatique de l'url d'un post/projet pour toujours avoir des url différentes même si plusieurs posts/projets on le même nom</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/e7ffa7e6fa4c07537014c0cf4e3ffd7a47a91de3">01/03/19</a> (encore)</h3>
	<ul>
		<li>Réorganisation de la gestion des url des pages :
			<ul>
				<li>Modification du fichier l3m-website-dist.conf, avec les bonnes règles d'urlrewriting</li>
				<li>Beauuucoup de changements dans les fichiers de classe d'admin, de blog & de projet</li>
				<li>Utilisation de l'extension IntL pour la fonction slugify() dans la classe Base (servant à générer les url)</li>
			</ul>
		</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/3e33e9c76cc1f335ff91be357168352791c4d3a2">01/03/19</a></h3>
	<ul>
		<li>Suppression de toutes les balises &lt;hr /&gt; (qui servent à séparer des paragraphes et pas des sections)</li>
		<li>Création du fichier adminController.php, avec les fonctions d'administration dedans</li>
		<li>Suppression du concept des commentaires. Après réflexion c'est plus simple pour les gens de réagir à un article du blog via leur site ou les réseaux sociaux</li>
		<li>Ajout d'un lien vers la section d'administration depuis toutes les pages du site si on est connecté</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/ebe5a9eaf0ab767e7e26d301df42db0674df5ad2">19/02/19</a></h3>
	<ul>
		<li>Remplacement de tous les guillemets double (") du code php par des guillemets simple (')</li>
		<li>Remplacement des "array()" du code php par "[]"</li>
		<li>Création d'un gitignore pour "/site/model/config.php"</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/919427bd2e687c0f5ffe6d83c9fe357e23e0ae1d">06/02/19</a></h3>
	<ul>
		<li>Ajout de commentaires au code</li>
		<li>Modification du fichier urlrewriting.txt (nouveau contenu = contenu du fichier nomdusite.conf de apache)</li>
		<li>Changement des classes BlogManager & ProjectManager ; déplacement du code depuis AdminManager</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/8f90b9e5bafe27fd6486ab3ce806762c4e86ec96">14/01/19</a> (encore)</h3>

	<ul>
		<li>Ajout d'un fichier "l3m.sql" pour montrer la base de données (supression du fichier sql.txt)</li>
		<li>Ajout d'un fichier "l3m-website-dist.conf" contenant un exemple de configuration pour le serveur apache.</li>
		<li>Ajout des pages détaillées de projets</li>
		<li>Modification de billet de blog fonctionnelle</li>
		<li>Modification des projets fonctionnelle</li>
		<li>Création de billet de blog</li>
		<li>Création de projet</li>
		<li>Suppression de billet de blog</li>
		<li>Suppression de projet</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/1d44e697386eb8066c6844c69c174fa141a7b20c">14/01/19</a></h3>

	<ul>
		<li>Modifications partie admin; ajout des listes des projets et des billets de blog</li>
		<li>Ajout de boutons pour poster un billet de blog ou un projet</li>
		<li>Changement de la structure des projets (voir bdd.txt), maintenant ils auront une description courte et une description longue accessible si on clique sur un lien particulier</li>
		<li>Changement de design de la page 404 (première utilisation du <i>!important</i>, berk)</li>
		<li>Sécurisation des pages (redirection vers le formulaire si accès incorrect)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/d38afd228a472bb750a26a221397761cb51d9c9b">13/01/19</a></h3>

	<ul>
		<li>Ajout du formulaire au panneau d'administration</li>
		<li>Stockage du hash du mdp dans le fichier config.php, utilisation de verify_hash() pour la connexion</li>
		<li>Affichage d'une vue sommaire pour le panneau d'administration</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/2ae140cc39ec43e1a9da520e988fc42d172d29c8">12/01/19</a></h3>

	<ul>
		<li>Modifications mineures css</li>
		<li>Modification du système MVC, le layout appelle les fonctions ou affiche les pages statique via un switch maintenant</li>
		<li>Suppression de quelques fichiers de fonctions pour les ajouter directement dans mainController.php</li>
		<li>Ajout d'une favicon ! <img src="/img/icon.png" alt="favicon morse" /></li>
		<li>Création des classes BlogManager & ProjectsManager</li>
		<li>Utilisation des classes créées pour récupérer les posts et projets dans la bdd</li>
		<li>Les vues sont maintenant séparées en deux dossiers; public et private. Les pages d'administration seront dans le dossier private.</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/bc7ded60ac9db636c949026f0c0deaad14cf04c4">11/01/19</a></h3>

	<ul>
		<li>Modifications css media queries</li>
		<li>Balise h2 enlevée de la page et mise dans le menu (comme ça ça fait menu & titre de page)</li>
		<li>Ajout css barre bleu/grise en dessous du header</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/e9f0366305c95549aaee98ba2485bc96255ce7f6">04/11/18</a></h3>

	<ul>
		<li>Changement des liens html (éviter de tout casser quand on descend dans l'arborescence)</li>
		<li>Unification des noms de classe css (camelCase)</li>
		<li>Design listes (notamment ici, dans le changelog)</li>
		<li>Un peu d'urlrewriting en plus de ce qu'il y a déjà (voir fichier urlrewriting.txt)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/ec56b1e9d9dfb2a1867c316a835423d7d119bf4c">26/10/18</a></h3>

	<ul>
		<li>Modif design pour écrans > 1500px</li>
		<li>Création bdd (voir fichier bdd.txt)</li>
		<li>Suppression de la page de connexion (inutile)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/26b343419dbe163e95f354283471a791f4545d59">22/10/18</a></h3>

	<ul>
		<li>Création & remplissage des pages <a href="changelog">changelog</a>, <a href="rss">rss</a> & <a href="404">404</a></li>
		<li>Modifs design du menu, ajout de la vidéo de fond sur la page 404</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/2aefac7867534e551e4d043fdb553231f9129d47">avant le 22/10/18</a></h3>

	<ul>
		<li>Création du layout MVC</li>
		<li>Création des pages vides Blog, Connexion, A propos, Contact, Projets</li>
		<li>Exemple de remplissage de Blog, remplissage de A propos & Contact</li>
		<li>Design sommaire</li>
		<li>Configuration Apache (url rewriting, site-enabled, toussa toussa)</li>
	</ul>
</article>

<article>
	<h3><a href="https://git.bitmycode.com/sodimel/l3m-website/commit/b1f971e8c23a05d452b730c35d1e5e9656d1c15a">Au tout début</a></h3>
	
	<p>
		Le 15 septembre 2018, j'ai eu envie de remplir mon site. Mais il était vraiment très mal codé.
	</p>

	<p>
		Donc là je me suis dit <q>okay cette année je refais mon site</q>. Et voilà.
	</p>

	<p>
		La v5 est la version de développement (il manque des features).
	</p>

</article>

<article>
	<p><i>Les <a href="https://git.bitmycode.com/sodimel/l3m-website">sources</a> sont disponibles sur le <a href="https://git.bitmycode.com">gitlab</a> de <a href="https://bitmycode.com">BitMyCode</a>.</i></p>	
</article>
