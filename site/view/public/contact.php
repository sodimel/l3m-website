<article>
	<h3>Mail</h3>

	<p>
		L'adresse à laquelle vous pouvez me contacter est la suivante:<br />
		corentin, signe arobase, <a href="https://244466666.xyz/">ce domaine</a> (<i>ça se lit un 2 trois 4 cinq 6, c'est rigolo hein ?</i>).
	</p>
</article>

<article>
	<h3>Vrac</h3>

	<ul>
		<li>
			<a rel="me" href="https://mamot.fr/@sodimel">Profil Mastodon</a>
		</li>
		<li>
			<a href="https://stackoverflow.com/users/6813732/sodimel">Profil stackoverflow</a>
		</li>
		<li>
			<a href="https://dev.to/corentinbettiol">Profil dev.to</a>
		</li>
		<li>
			<a href="https://www.linkedin.com/in/corentinbettiol/">Profil linkedin</a>
		</li>
	</ul>
</article>

<article>
	<h3>Du code ?</h3>

	<ul>
		<li>
			<a href="https://git.bitmycode.com/sodimel/">Profil git.bitmycode.com</a>, l'instance gitlab de <a href="https://bitmycode.com">BitMyCode</a>.
		</li>
		<li>
			<a href="https://gitlab.com/sodimel/">Profil gitlab</a>
		</li>
		<li>
			<a href="https://github.com/corentinbettiol/">Profil github</a> (comme gitlab mais en moins bien)
		</li>
		<li>
			<a href="https://codepen.io/lmmm/">Profil codepen</a>, parce que le CSS c'est rigolo
		</li>
	</ul>
</article>
