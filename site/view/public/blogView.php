<?php
	if(!isset($pageName))
	{
		header('Location:/');
		exit();
	}

	foreach ($blogposts as $blogpost)
	{
		$blogpost['datetime'] = new DateTime($blogpost['time']);

		$tag = new TagManager();
		$blogpost['tags'] = $tag->getTags($blogpost['id']);
		$exist = true;
		include('site/view/public/blogPostView.php');
	}
	if(!isset($exist))
	{
		?>
		<article>
			<h3>Pas de post</h3>

			<p>Aucun billet n'a encore été publié, patience !</p>
		</article>
		<?php
	}
