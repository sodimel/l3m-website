<article>
	<h3>404 - Page not found</h3>
	<p>
		Ce lien est cassé.
	</p>
	<p>
		Le contenu n'existe pas ou a changé d'adresse, ou alors vous vous amusez avec les url. Dans les deux cas il n'y a rien à trouver ici.
	</p>
</article>

<div id="videoFond">
	<video autoplay loop muted>
		<source type="video/mp4" src="/img/CloudsLoop.mp4" media="(orientation: landscape)" />
	</video>
</div>
