<?php
	if(!isset($pageName)){
		header('Location:/');
		exit();
	}
?>
<nav class="paging">
<?php

	// flèche gauche
	if($paging['first'])
		echo '<a href="/page/1" title="Première page du blog">&#8606;️</a>';
	else
		echo '<em>&#8606;️</em>'; // ↞

	// pages précédentes
	for($i = 0; $i < 2 - count($paging['before']); $i++)
		echo '<span></span>';

	foreach ($paging['before'] as $b)
		echo '<a href="/page/'. $b .'" title="Page '. $b .' du blog">'. $b .'</a>';

	// page courante
	echo '<em class="current">'. $paging['current']. '</em>';


	// pages suivantes
	foreach ($paging['after'] as $a)
		echo '<a href="/page/'. $a .'" title="Page '. $a .' du blog">'. $a .'</a>';

	for($i = 0; $i < 2 - count($paging['after']); $i++)
		echo "<span></span>";

	// flèche droite
	if($paging['last'])
		echo '<a href="/page/'. $paging['last'] .'" title="Dernière page du blog">&#8608;️</a>';
	else
		echo '<em>&#8608;️</em>'; // ↠
?>
</nav>
