<?php
	if(!isset($pageName)){
		header('Location:/');
		exit();
	}

	require_once('site/libs/Parsedown.php');
	require_once('site/libs/ParsedownExtra.php');
	$parsed = new ParsedownExtra();

	// thx https://iovs.arvojournals.org/article.aspx?articleid=2166061
	// french = 249 wpm
	$nbwords = str_word_count($project['content']);
	$readingtime = floor($nbwords/249);
	if($readingtime < 2)
		$readingtime = "moins de 2";

?>
	<article <?php if($project['cover']) { echo 'class="covercontainer"'; } else { echo 'class="simple"'; } ?>>
		<section <?php if($project['cover']) { echo 'class="cover"'; } ?>>
			<?php if($project['cover']) { ?>
				<img src="<?php echo $project['cover']; ?>" class="coverImg" alt="cover for <?php echo $project['name']; ?>" loading="lazy" />
			<?php } ?>
			<h3><a href="/project/<?php echo $project['slug']; ?>"><?php echo $project['name']; ?></a></h3>
			<p class="postInfos">
				<a href="<?php echo $project['link']; ?>">Lien vers le projet</a> &mdash; 📖 <?php echo $readingtime; ?> min
			</p>
		</section>
			<?php
				if (!in_array($project['slug'], array("npy", "another-static-site-builder", "twinoid-archiver", "suicnw", "default-python-package-builder", "djangocheckseo", "vieux-projets-twinoid", "liste-des-vieux-projets")))
					echo $parsed->text(html_entity_decode($project['content']));
				else
					echo htmlspecialchars_decode($project['content']);
			?>
	</article>
