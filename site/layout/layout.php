<!DOCTYPE html>
<html lang="fr" prefix="og: https://ogp.me/ns#">
<head>
	<?php
	/*
	** TODO: mettre en place une fonction getObject() quelque part qui récupère l'objet de la page courante en fonction de l'url
	** pour pouvoir afficher le og title/description/image simplement sans faire chier, là on récup l'objet plus tard (au moment
	** de l'afficher), c'est nul :(
	*/
	?>
	<meta charset="utf-8" />
	<title><?php echo $pageTitle; // voir getPageTitle() dans mainController ?></title>
	<meta property="og:title" content="<?php echo $pageTitle; ?>" />
	<meta name="description" content="<?php echo $description; ?>" />
	<meta name="viewport" content="width=device-width" />
	<link rel="preload" as="font" href="/fonts/sharetechmono.woff2" type="font/woff2" crossorigin="anonymous">
	<link rel="preload" as="style" href="/css/minified.css" /><!-- readable design file in /css/design.css -->
	<link rel="stylesheet" href="/css/design.css" />
	<?php if($pageName[1] == 404){ // le css pour la page 404 ?>
		<link rel="stylesheet" href="/css/404.css" />
		<link rel="preload" as="style" href="/css/404.css" />
	<?php } ?>
	<?php if($pageName[1] == "blog" && isset($_GET['title'])){ // open graph preview image !
		$blogpost = getBlogPost($_GET['title']);
		if($blogpost){
		?>
			<meta property="og:description" content="<?php print(str_replace('"', "'", $blogpost["shortContent"])); ?>" />
			<meta property="og:image" content="<?php print($blogpost["cover"]); ?>" />
			<meta name="twitter:card" content="<?php print($blogpost["cover"]); ?>" />
		<?php
			}
	}
	?>
	<?php if($pageName[1] == "project" && isset($_GET['title'])){
		$project = getProject($_GET['title']);
		if($project){
		?>
			<meta property="og:description" content="<?php print(str_replace('"', "'", $project["shortContent"])); ?>" />
			<meta property="og:image" content="<?php print($project["cover"]); ?>" />
			<meta name="twitter:card" content="<?php print($project["cover"]); ?>" />
		<?php
			}
	}
	?>
	<link rel="icon" type="image/png" href="/img/icon.png" />
	<meta name="monetization" content="$ilp.uphold.com/qdwi44DqP7a9">
	<style>
		*{
			color: #1f1f2e;
		}
		#listMenu a{
			background-color: #ddd;
			padding: 7px;
		}
		@media all and (max-width: 700px){
			#listMenu a{
				padding: 2px 2px 5px 2px;
			}
		}
	</style>
</head>
<body id="page">
	<header id="header">
		<section id="title">
			<a href="/" title="Accueil - blog"><!--
				--><img src="/img/logo.png" alt="Logo l3m website (morse pixel art)" id="logo" /><!--
			--></a><!--
			--><h1>l3m website</h1>
		</section>
		<nav id="menu">
			<?php writeMenu($pageName[1]); // voir mainController ?>
		</nav>
	</header>

	<?php
	// le gros switch qui fait peur

	switch ($pageName[1]){
		case 'blog': // blog
			writeBlogPage($pageName); // voir mainController
			break;
		case 'about': // à propos
			require('site/view/public/about.php'); // statique
			break;
		case 'project': // projet
			writeProjectPage($pageName); // voir mainController
			break;
		case 'eplucher': // déconnexion du panneau d'administration
			decoAdminPage(); // voir mainController
		case 'contact': // contact
			require('site/view/public/contact.php'); // statique
			break;
		case 'changelog': // historique de développement
			require('site/view/public/changelog.php'); // statique
			break;
		case 'rss': // rss
			require('site/view/public/rss.php'); // todo
			break;
		case '404': // page not found
			require('site/view/public/404.php'); // statique
			break;
		case 'administration': // panneau d'administration (url = patate)
			// si on envoie les données en post
			if(isset($_POST['user']) && isset($_POST['pass'])){
				loginAdminPage($pageName);
			}
			// afficher le login screen
			else{
				writeAdminPage($pageName);
			}
			break;
		case 'tag':
			writeTagPage($pageName);
			break;
		default: // page non connue (n'est pas senser arriver)
			header('Location:/404');
			break;
	} ?>

	<footer id="footer">
		<p>
			Site en vanilla html5/css3/php7, fait avec ♥
		</p>
		<p>
			<a href="/changelog" title="changelog">Changelog</a> - <a href="/rss" title="rss">RSS</a> - <a href="https://links.l3m.in/">Instance share links</a> - v5
		</p>
	</footer>
</body>
</html>
