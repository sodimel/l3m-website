<?php

// fichier de config pour la bdd & le compte pour la partie administrateur
require_once('site/model/config.php');

// connexion à la base de données
require_once('site/model/Base.php');

// classe pour la gesion des billets de blog
require_once('site/model/BlogManager.php');

// classe pour la gestion des projets
require_once('site/model/ProjectsManager.php');

// classe pour la gestion des tags
require_once('site/model/TagManager.php');

// récupérer le nom de la page en fonction de l'url
// premier texte = affiché dans l'onglet (title), second texte = voir le switch dans site/layout/layout.php :
// 	c'est LA valeur utilisée pour savoir quoi faire depuis le layout
function getPageName()
{
	// si on a une url personnalisée
	if(isset($_GET['action']))
	{
		switch ($_GET['action'])
		{
			case 'blog' : // le blog
				if(isset($_GET['page']))
					return ['Blog &mdash; page '. (int)$_GET['page'], 'blog'];
				else
					return ['Blog', 'blog'];
				break;
			case 'contact': // contact
				return ['Contact', 'contact'];
				break;
			case 'about': // à propos
				return ['À Propos', 'about'];
				break;
			case 'projects': // projets
				return ['Projets', 'project'];
				break;
			case 'project': // projet
				return ['Projet', 'project'];
				break;
			case 'changelog': // historique des versions
				return ['Changelog', 'changelog'];
				break;
			case 'eplucher': // déconnexion (la première valeur n'est jamais utilisée)
				return ['POIL AU NEZ', 'eplucher'];
				break;
			case 'rss': // rss
				return ['Rss', 'rss'];
				break;
			case '404': // page not found
				return ['Erreur 404 - File not found', '404'];
				break;
			case 'patate': // administration
				return ['Administration', 'administration'];
				break;
			case 'tags': // voir les posts de blog avec le tag
				return ['Tag', 'tag'];
				break;
			default: // si l'argument n'est pas compris et qu'il passe les rewriterules d'apache on affiche le blog (ne devrait jamais se produire)
				return ['Blog', 'blog'];
				break;
		}
	}
	// si on est sur l'accueil du site
	else
		return ['Blog', 'blog'];
}

// récupérer le contenu de la balise <title>
function getPageTitle()
{
	if(isset($_GET['action']))
	{
		switch ($_GET['action'])
		{
			case 'blog' : // le blog
				if(isset($_GET['title'])){
					$blog = new BlogManager(null, null, null, null, null, null, null, null, null); // on a un blogmanager
					$title = $blog->getTitle($_GET['title'])['title'];
					if(!$title)
					{
						$title = "Undefined article";
					}
				}
				else{
					if(isset($_GET['page']))
						$title = "Blog | Page ". (int)$_GET['page'];
					else
						$title = "Blog";
				}
				break;
			case 'contact': // contact
				$title = "Contact";
				break;
			case 'about': // à propos
				$title = "À propos";
				break;
			case 'projects': // projets
				$title = "Projets";
				break;
			case 'project': // projet
				if(isset($_GET['title'])){
					$project = new ProjectManager(null, null, null, null, null, null, null, null); // on a un projectmanager
					$title = $project->getName($_GET['title'])['name'];
					if(!$title)
					{
						$title = "Undefined project";
					}
				}
				break;
			case 'changelog': // historique des versions
				$title = "Changelog";
				break;
			case 'rss': // rss
				$title = "Flux RSS";
				break;
			case '404': // page not found
				$title = "Page 404";
				break;
			case 'tags': // voir les posts de blog avec le tag
				$title = "Tag ". $_GET['title'];
				break;
			case 'patate': // administration
				$title = "Administration";
				break;
			default: // si l'argument n'est pas compris et qu'il passe les rewriterules d'apache on affiche le blog (ne devrait jamais se produire)
				$title = "Undefined page";
				break;
		}
	}
	else
		$title = "Blog";

	return  $title ." &ndash; l3m website";
}

function getDescription()
{
	// si on a une url personnalisée
	if(isset($_GET['action']))
	{
		switch ($_GET['action'])
		{
			case 'blog' : // le blog
				if(isset($_GET['title'])){
					$blog = new BlogManager(null, null, null, null, null, null, null, null, null); // on a un blogmanager
					$title = $blog->getTitle($_GET['title'])['title'];
					if(!$title)
					{
						$title = "Undefined article";
					}
					return $title .", sur le blog du l3m website.";
				}
				else{
					if(isset($_GET['page'])){
						return "La page n°". (int)$_GET['page'] ." du blog.";
					}
					return "Le blog permet d'avoir accès aux derniers messages postés, et de naviguer parmis eux.";
				}
				break;
			case 'contact': // contact
				return "La page de contact liste tous les moyens de contact que je mets à disposition.";
				break;
			case 'about': // à propos
				return "La page à propos indique quelques informations sur le créateur de ce site, sur le site & sur le serveur qui l'héberge.";
				break;
			case 'projects': // projets
				return "La liste de mes projets, que je partage ici afin de ne pas avoir des liens qui traînent de partout.";
				break;
			case 'project': // projet
				if(isset($_GET['title'])){
					$project = new ProjectManager(null, null, null, null, null, null, null, null); // on a un projectmanager
					$description = $project->get($_GET['title'])['shortContent'];
					if(!$description)
					{
						$description = "Des informations à propos du projet nommé \"". $project->get($_GET['title'])['name'] ."\".";
					}
				}
				else{
					$description = "Unknown project.";
				}
				return $description;
				break;
			case 'changelog': // historique des versions
				return "L'historique des versions du site, ne devrait plus beaucoup évoluer maintenant que le site est terminé.";
				break;
			case 'rss': // rss
				return "La page qui liste les différents flux rss disponibles.";
				break;
			case '404': // page not found
				return "Page 404, le contenu n'a pas pu être trouvé sur ce site.";
				break;
			case 'tags': // voir les posts de blog avec le tag
				return "La page de visualisation des billets de blog contenant le tag ". $_GET['title'] .".";
				break;
			default: // si l'argument n'est pas compris et qu'il passe les rewriterules d'apache on affiche le blog (ne devrait jamais se produire)
				return "Site perso de Corentin Bettiol.";
				break;
		}
	}
	// si on est sur l'accueil du site
	else
		return "Le blog du l3m website, avec des articles et du contenu.";
}

// affiche le menu du site (avec le titre de la page)
function writeMenu($pageName)
{
?>
	<input id="toggle" type="checkbox" />
	<ul id='listMenu'>
		<?php
			if($pageName == 'administration'){ ?>
				<li><h2><a href="/eplucher" class="selected">🔧</a></h2></li>
		<?php }
			else if(isset($_SESSION['user'])){ ?>
				<li><h2><a href="/patate" >🔧</a></h2></li>
		<?php }
			if($pageName == 'changelog'){ ?>
				<li><h2><a href="/changelog" class="selected">Changelog</a></h2></li>
		<?php }
			if($pageName == 'tag'){ ?>
				<li><h2><a href="/tag/<?php echo $_GET['title']; ?>" class="selected">Tag <?php echo $_GET['title']; ?></a></h2></li>
		<?php } ?>
		<li><h2><a href="/" title="Blog" <?php if($pageName == 'blog'){ ?>class="selected"<?php } ?>>Blog</a></h2></li>
		<li><h2><a href="/about" title="À propos" <?php if($pageName == 'about'){ ?>class="selected"<?php } ?>>À propos</a></h2></li>
		<li><h2><a href="/projects" title="Projets" <?php if($pageName == 'project'){ ?>class="selected"<?php } ?>>Projets</a></h2></li>
		<li><h2><a href="/contact" title="Contact" <?php if($pageName == 'contact'){ ?>class="selected"<?php } ?>>Contact</a></h2></li>
	</ul>
	<label class="nav-label" for="toggle"></label>
<?php }

function getBlogPost($pageName)
{
	$blog = new BlogManager(null, null, null, null, null, null, null, null, null); // on a un blogmanager

	// si on affiche un post en particulier
	if(isset($_GET['title']))
	{
		$blogpost = $blog->get($_GET['title']);
		if($blogpost != false)
		{
			return $blogpost;
		}
		else
		{
			return null;
		}
	}
	return null;
}

// affiche la page du blog (accueil par défaut)
function writeBlogPage($pageName, $blogpost = null)
{
	if($blogpost){
		require('site/view/public/blogPostView.php');
	}
	$blog = new BlogManager(null, null, null, null, null, null, null, null, null); // on a un blogmanager

	// si on affiche un post en particulier
	if(isset($_GET['title']))
	{
		$blogpost = $blog->get($_GET['title']);
		if($blogpost != false)
		{
			// on affiche l'article si on l'a trouvé
			require('site/view/public/blogPostView.php');
		}
		else
			header('Location:/404');
	}
	// si on affiche la liste des posts
	else
	{
		$page = 1;
		if(isset($_GET['page']))
			// normalement via le rewriterule on envoie forcément un int, mais un cast ça coûte pas grand chose donc bon
			$page = (int)$_GET['page'];

		// liste des pages au dessus des articles
		$paging = $blog->pagingList($page);
		require('site/view/public/blogPagingView.php');

		// liste des articles de la page
		$blogposts = $blog->getPage($page);
		require('site/view/public/blogView.php');


		// liste des pages au dessous des articles
		$paging = $blog->pagingList($page);
		require('site/view/public/blogPagingView.php');
	}
}

function getProject($pageName)
{
	$project = new ProjectManager(null, null, null, null, null, null, null, null); // on a un projectmanager

	// si on affiche un projet en particulier
	if(isset($_GET['title']))
	{
		$project = $project->get($_GET['title']);
		if($project != false)
		{
			return $project;
		}
		else
		{
			return null;
		}
	}
	return null;
}

// affiche la liste des projets
function writeProjectPage($pageName, $project = null)
{
	if($project){
		require('site/view/public/projectView.php');
	}
	$project = new ProjectManager(null, null, null, null, null, null, null, null); // on a un projectmanager

	// si on affiche un projet en particulier
	if(isset($_GET['title']))
	{
		$project = $project->get($_GET['title']);
		if($project != false)
		{
			require('site/view/public/projectView.php');
		}
		else
			header('Location:/404');
	}
	// si on affiche la liste des projets
	else{
		$projects = $project->getProjects();
		require('site/view/public/projectsView.php');
	}
}

// affiche la liste des posts de blog avec le tag correspondant
function writeTagPage($pageName){
	$blog = new BlogManager(null, null, null, null, null, null, null, null, null); // on a un blogmanager

	// liste des articles avec le tag
	if(isset($_GET['title']))
	{
		$blogposts = $blog->getTag($_GET['title']);
		// zéro post = modif d'url
		if(!$blogposts)
			header("Location:/");
		// on a des posts!
		else
			require('site/view/public/blogView.php');
	}
	else
		header("Location:/");
}
