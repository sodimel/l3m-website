<?php

// classe pour l'administration du contenu du site
require_once('site/model/AdminManager.php');

// classe pour la màj des fichiers RSS
require_once('site/model/RssManager.php');
// classe pour la màj du sitemap
require_once('site/model/SitemapManager.php');

// tente de connecter l'administrateur
function loginAdminPage($pageName)
{
	if(password_verify($_POST['pass'], pass) && $_POST['user'] == user)
	{
		$_SESSION['user'] = user;
		header('Location:/patate');
	}
	else
		header('Location:/patate');
}

// afficher le panneau d'administration & gère l'administration du site
function writeAdminPage($pageName)
{
	$admin = new AdminManager();

	if($admin->verifyAdminConnect())
	{
		if(isset($_GET['type']))
		{
			switch ($_GET['type'])
			{
				case 'blogedit':
					if(!isset($_POST['title']))
					{
						$title = htmlspecialchars($_GET['title'] ?? '');
						if($blogpost = $admin->getBlogPost($title))
							require('site/view/private/editblogView.php');
						else
							header('Location:/404');
					}
					else{
						$admin->updateBlogPost();
						header('Location:/patate');
					}
					break;

				case 'blogsubmit':
					if(isset($_POST['title']))
					{
						$admin->sendBlogPost();
						header('Location:/patate');
					}
					else
						require('site/view/private/createBlogView.php');
					break;

				case 'blogdelete':
					if(isset($_GET['title']))
					{
						$admin->deleteBlogPost($_GET['title']);
						header('Location:/patate');
					}
					break;

				case 'projectedit':
					if(!isset($_POST['name']))
					{
						$title = urldecode($_GET['title']);
						$project = $admin->getProject($title);
						require('site/view/private/editProjectView.php');
					}
					else{
						$admin->updateProject();
						header('Location:/patate');
					}
					break;

				case 'projectsubmit':
					if(isset($_POST['name']))
					{
						$admin->sendProject();
						header('Location:/patate');
					}
					else
						require('site/view/private/createProjectView.php');
					break;

				case 'projectdelete':
					if(isset($_GET['title']))
					{
						$admin->deleteProject($_GET['title']);
						header('Location:/patate');
					}
					break;

				default:
					# code...
					break;
			}
		}
		else{
			$blogposts = $admin->getBlogPosts();
			$projects = $admin->getProjects();

			require('site/view/private/admin.php');
		}
	}
	else
		require('site/view/private/loginFormView.php');
}

// déconnexion de la partie administration
function decoAdminPage()
{
	$_SESSION = array();
	header('Location:/');
}
